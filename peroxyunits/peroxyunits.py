__name__ = 'peroxyunits'

# PRESSURE

def bar():
    return 100000


def atm():
    return 101325


def psi():
    return 6894.75729


def mbar():
    return 100


def torr():
    return 133.322368

# CONSTANTS

def R():
    return 8.3144699

def Na():
    return 6.02214076e+23

# ENERGY

def cal():
    return 4.184

def btu():
    return 1055.05585

def kJ():
    return 1000.

# MASS

def lb():
    return 0.45359237

# TEMPERATURE

def F2C(F):
    return (F - 32) / 1.8


def F2K(F):
    return F_to_C(F) + 273.15


def K2F(K):
    return 1.8 * (K - 273.15) + 32


def K2C(K):
    return K - 273.15


def C2K(C):
    return C + 273.15


def C2F(C):
    return 1.8 * C + 32

# CONCENTRATION FUNCTIONS

def m2w(x):
    return 34.015 * x / (16. * x + 18.015)


def w2m(w):
    return 18.015 * w / (34.015 - 16. * w)


def molar_mass(x):
    return 34.015 * x + 18.01528 * (1 - x)


def molar_mass_w(w):
    x = w2m(w)
    return molar_mass(x)

