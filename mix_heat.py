import time as t
from peroxylib import *
from peroxylib import ac as ac_semi
from scipy.misc import derivative
from scipy.optimize import curve_fit
from scipy import interpolate as ip
import numpy
from random import random as rand
from peroxyunits import *
import legacy

def fun(X, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O):

    x = X[0]
    T = X[1]

    xs = 2*x - 1

    return x*(1-x)*(A + B*x + C*x**2 + (D + E*T + F*T**2 + G*T**3) * numpy.log(x + H))

Tt = [0.] * 11 + [25.] * 11 + [45.] * 11 + [60.] * 11 + [75.] * 11

X0 = [0., 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.]
Xt = X0 * 5

H0 = [590., 640., 667., 667., 641., 594., 515., 417., 295., 157., 0.]
H25 = [815., 807., 787., 753., 704., 637., 545., 436., 306., 162., 0.]
H45 = [950., 907., 856., 793., 729., 654., 556., 443., 310., 163., 0.]
H60 = [1030., 986., 888., 803., 741., 663., 561., 447., 313., 164., 0.]
H75 = [1110., 1045., 900., 820., 750., 670., 565., 450., 315., 165., 0.]

Ht = H0 + H25 + H45 + H60 + H75

Ta = numpy.array(Tt)
Xa = numpy.array(Xt)
Ha = numpy.array(Ht)


Ta = C2K(Ta)
Xa = w2m(Xa)

Ha = cal() * Ha
Ha = Ha * Xa
Ha = -Ha

params = [-3.72176514e+03, -1.69363541e+03,  1.80999213e+03, -2.94864163e+04,
  2.49618569e+02, -7.07502142e-01,  6.75549199e-04,  7.20630910e-08,
  1.00000000e+00,  1.00000000e+00,  1.00000000e+00,  1.00000000e+00,
  1.00000000e+00,  1.00000000e+00,  1.00000000e+00]

vals = curve_fit(fun, (Xa, Ta), Ha, p0 = params)[0]

def fun2(X, T):

    return fun((X, T), vals[0], vals[1], vals[2], vals[3], vals[4], vals[5], vals[6], vals[7], vals[8], vals[9], vals[10], vals[11], vals[12], vals[13], vals[14])

def fun3(T):

    return fun2(x, T)

Oldz = numpy.array([])
Results = numpy.array([])

for i in range(0, 55):

    if Ha[i] != 0:

        Oldz = numpy.append(Oldz, Ha[i])
        Results = numpy.append(Results, fun2(Xa[i], Ta[i]))

Errors = Results - Oldz
Relatives = Errors / Oldz

print (numpy.mean((((Relatives)))))


print (vals)

for xind in range(0,101):
    x = float(xind)/100.
    print (derivative(fun3, C2K(0)), derivative(fun3, C2K(20)), derivative(fun3, C2K(40)), derivative(fun3, C2K(60)))