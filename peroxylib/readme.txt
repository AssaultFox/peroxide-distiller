Peroxylib v 0.6

This library was created and developed with sole purpose of enabling simulation of fractionating column in a non simplified model.

"Solution" reffers to aqueous solution of hydrogen peroxide.
"Concentration" reffers to molar part of hydrogen peroxide in solution.
All concentrations ale molar, unless stated otherwise.
Default units are:

Pressure - Pascal
Temperature - Kelvin
Concentration - no unit (0-1)

All formulas for chemical and physical properties of hydrogen peroxide are derived from Hydrogen Peroxide Handbook, ROcketdyne, 1967.
Various numerical solutions are employed to solve for otherwise incalculable values.

Created by K. Wroblewski.


### SETTINGS ###

prec_T(p_T)

	Sets precision of calculations of temperature to 'p_T'.


prec_P(p_P)

	Sets precision of calculations of pressure to 'p_P'.


prec_h(p_h)

	Sets step of differential solver to 'p_h'.


prec_c(p_c)

	Sets precision of calculations of condensate composition to 'p_c'.


prec_default()

	Sets precision of calculations to default. Defaults precisions are:

		p_T = 0.01
		p_P = 0.1
		p_h = 0.001
		p_c = 0.00001
	
	NOTE: This function, or all of precision setting functions must be called before using peroxylib.



### NUMERICS ###

NOTE: Those functions are for internal use for peroxylib and are not imported.


parabola(X_, Y_)

	Takes two lists, each of 3 point coordinates.
	Returns a tuple of second degree polynomial coefficients, which fit input point coordinates.


sqf(coeff, x)

	Takes a list of three coefficients and a number.
	Returns a value of second degree polynomial with coefficients and number given.


parabolic(points, x)

	Takes a list, items of which are lists of x and y coordinates respectively, and a number.
	Returns parabolic interpolation of the function given by 'points' in 'x' point.


rk4(x, y, h, f)

	A Runge-Kutta function of fourth order.


bisec(a_, b_, delta, f)

	A bisection function for solving complex equations.
	Takes boundaries of a section, in which the solution for f(x) = 0 will be looked for, required precision, and the function.



### BASIC FUNCTIONS ###

bar()

	Returns one bar in pascals.


torr()
	
	Returns one torr in pascals.


F_to_C(F)

	Converts temperature from Fahrenheit to Celcius.


F_to_K(F)

	Converts temperature from Fahrenheit to Kelvin.


K_to_F(K)

	Converts temperature from Kelvin to Fahrenheit.


K_to_C(K)

	Converts temperature from Kelvin to Celcius.


C_to_K(C)

	Converts temperature from Celcius to Kelvin.


C_to_F(C)

	Converts temperature from Celcius to Fahrenheit.


antoine(A, B, C, T)

	Returns value of pressure by antoine formula with coefficients 'A', 'B', 'C' in temperature 'T'.


rev_antoine(A, B, C, P)

	Reverses the antoine equation, returning temperature required for pressure 'P' by antoine formula with coefficients 'A', 'B', 'C'.


molar_to_mass(x)

	Converts molar concentration of aqueous solution of hydrogen peroxide to mass concentration.


mass_to_molar(m)

	Converts mass concentration of aqueous solution of hydrogen peroxide to molar concentration.
	

molar_mass(x)

	Returns molar mass of an aqueous hydrogen peroxide solution of molar concentration 'x'.


molar_mass_w(m)

	Returns molar mass of an aqueous hydrogen peroxide solution of mass concentration 'm'.


density(x, T):

	Returns density of an aqueous hydrogen peroxide solution of molar concentration 'x' in temperature 'T'.
	Currently imprecise.



### PHYSICAL FUNCTIONS ###

w_vap_press(T)

	Returns pressure of saturated water vapour in temperature 'T'.
	Uses Antoine formula.


hp_vap_press(T)

	Returns pressure of saturated hydrogen peroxide vapour in temperature 'T'.
	Uses expanded Antoine formula.


w_vap_temp(P)

	Returns temperature of saturated water vapour in pressure 'P'.
	Uses reversed Antoine formula.


hp_vap_temp(P)

	Returns temperature of saturated hydrogen peroxide vapour in pressure 'P'.
	Uses reversed expanded Antoine formula.


ac(x, T)

	Returns a list containing:

	[0] Chemical activity coefficient of hydrogen peroxide in aqueous solution of concentration 'x' in temperature 'T'.	
	[1] Chemical activity coefficient of water in aqueous solution of concentration 'x' in temperature 'T'.

	Uses parabolic interpolation of data array.


sol_press(x, T)

	Returns pressure of saturated vapour above solution of composition 'x' in temperature 'T'.


sol_temp(x, P)

	Returns temperature in which solution of composition 'x' boils under pressure 'P'.


vap_press(y, T):

	Returns pressure of saturated vapour of composition 'y in temperature 'T'.

	References vap_comp_T as to numerically find correspoding solution concentration.	


vap_temp(y, P)

	Returns temperature of saturated vapour of composition y under pressure 'P'.

	References vap_comp_P as to numerically find correspoding solution concentration.	


vap_comp_T(x, T)

	Returns concentration of vapour above solution of concentration 'x' in temperature 'T'.

vap_comp_P(x, P)

	Returns concentration of vapour above solution of concentration 'x' under pressure 'P'.

vap_heat_T(x, T)

	Returns heat of evaporation for solution of composition 'x' in temperature 'T'.

	Returned value reffers to one mole of evaporated liquid.

vap_heat_P(x, P)

 	Returns heat of evaporation for solution of composition 'x' under pressure 'P'.

	Returned value reffers to one mole of evaporated liquid..

cond_heat_T(y, T)

	Returns heat of condensation for vapour of composition 'y' in temperature 'T'.
	
	Returned value reffers to one mole of condensated vapour.

cond_heat_P(y, P)

	Returns heat of condensation for vapour of composition 'y' under pressure 'P'.

	Returned value reffers to one mole of condensated vapour.

cond_T(y, T)

	Returns concentration of liquid equlibrical to vapour of composition 'y' in temperature 'T'.

	References vap_comp_T as to solve for liqud concentration.

cond_P(y, P)

	Returns concentration of liquid equlibrical to vapour of composition 'y' under pressure 'P'.

	References vap_comp_P as to solve for liqud concentration.

condensate_to_T(y, P, T)

	Simulates condensation of vapour by cooling it to temperature 'T'.
	Returns a list of three items:

	[0] Concentration of vapour of initial composition 'y' after cooling down to temperature 'T'.
	[1] Heat released per mole of cooled vapour.
	[2] Molar amount of condensate liquid acquired from the cooling process as related to initial vapour.


condensate(y, P, eff)

	Simulates condensation of vapour by condensating eff moles out of each mole of initial vapour.
	Returns a list of two items:

	[0] Concentration of vapour of initial composition 'y' after cooling down to temperature 'T'.
	[1] Molar amount of condensate liquid acquired from the cooling process as related to initial vapour.
