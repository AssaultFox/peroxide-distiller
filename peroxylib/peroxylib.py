# Peroxylib v.0.7 -> 0.8
# Now featuring serious numerics!

from math import *
from scipy.integrate import odeint
import numpy as np
import scipy.interpolate as ip
import scipy.optimize as opt
from scipy.misc import derivative
from peroxyunits import *


__name__ = 'peroxylib'

############## Settings ##############

def method(compsolv ='brenth', tempsolv ='brenth', presssolv ='brenth', lo_temp_ = 260, hi_temp_ = 430):

    global compsolver
    global tempsolver
    global presssolver
    global lo_temp
    global hi_temp

    compsolver = compsolv
    tempsolver = tempsolv
    presssolver = presssolv
    lo_temp = lo_temp_
    hi_temp = hi_temp_


def prec_h(p_h_):

    global p_h
    p_h = p_h_


prec_h(0.001)
method()


############## Basic functions ##############

def w_density(T):

    Tc = K2C(T)

    val = 999.83952
    val = val + 16.945176 * Tc
    val = val - 7.9870401e-3 * Tc * Tc
    val = val - 46.170461e-6 * Tc * Tc * Tc
    val = val + 105.56302e-9 * Tc * Tc * Tc * Tc
    val = val - 280.54253e-12 *Tc * Tc * Tc * Tc * Tc
    val = val /(1 + 16.897850e-3 * Tc)

    return val/1000.


def density(x, T):

    a = w_density(T)
    xw = m2w(x)
    Tc = K2C(T)

    arr1 = [
        [ 0.39763, -2.8732e-3,  3.2488e-5, -1.6363e-7],
        [ 0.02206,  3.5357e-3, -6.0947e-5,  3.6165e-7],
        [ 0.05187, -1.9414e-3,  3.9061e-5, -2.5500e-7]
        ]

    arr2 = []

    for i in range(0, 3):

        val = arr1[i][0] + arr1[i][1]*Tc + arr1[i][2]*Tc*Tc + arr1[i][3]*Tc*Tc*Tc

        arr2.append(val)

    return a + arr2[0] * xw + arr2[1] * xw * xw + arr2[2] * xw * xw * xw


def den_comp(d, T):

    def fun(x):

        return density(x, T) - d

    return opt.bisect(fun, 0, 1)


############## Excess functions ##############

def excess_gibbs(x, T):

    A = -4613.8279896
    B = 519.1382624
    C = 293.8685136
    D = 4.397808665
    E = -0.48379259

    xw = 1 - x

    return x * xw * (A + B*x + C*x**2 + D*T + E*x*T)


def ac(x, T):

    A = -4613.8279896
    B = 519.1382624
    C = 293.8685136
    D = 4.397808665
    E = -0.48379259
    RT = T * R()

    xw = 1 - x

    u1 = xw**2 * (A + B*x + C*x**2 + D*T + E*x*T )
    u1 = u1 + x*xw*xw*(B + 2*C*x + E*T)

    u2 = x**2 * (A + B*x + C*x**2 + D*T + E*x*T )
    u2 = u2 - x*x*xw*(B + 2*C*x + E*T)

    return [exp(u1/RT), exp(u2/RT)]


def mix_heat(x, T):

    A = -3.72176514e+03
    B = -1.69363541e+03
    C = 1.80999213e+03
    D = -2.94864163e+04
    E = 2.49618569e+02
    F = -7.07502142e-01
    G = 6.75549199e-04
    H = 7.20630910e-08

    return x*(1-x)*(A + B*x + C*x**2 + (D + E*T + F*T**2 + G*T**3) * np.log(x + H))



def mix_entropy(x, T):

    return (mix_heat(x, T) - excess_gibbs(x, T))/T

############## Equilibrium functions ##############

def w_vap_press(T):     #using Buck equation now

    Tc = float(T) - 273.15
    val = (18.678 - Tc/234.5)*(Tc/(257.14 + Tc))
        
    return 611.21*exp(val)
        
        
def hp_vap_press(T_):
    
    T = float(T_)
    Ti = 1./T
    e = -2587.66*Ti + 11.13630
    P = pow(10, e)
    
    return P
        
        
def w_vap_temp(P): # TO BE FIXED
    
    A = 10.19621
    B = 1730.63 
    C = 233.426 
    
    return C2K(rev_antoine(A, B, C, P))
        
        
def hp_vap_temp(P): # TO BE FIXED
    
    P_ = P/133.322
    
    a = -24675.
    b = -2482.6
    c = 8.92536 - log(P_, 10)
    
    delta = b*b - 4*a*c
    sdelta = sqrt(delta)    

    x = 2*a / (-b - sdelta)

    return x


def sol_press(x_, T):
    
    ac_ = ac(x_, T)
    
    return hp_vap_press(T)*x_*ac_[0] + w_vap_press(T)*(1.-x_)*ac_[1] 
         
        
def sol_temp(x_, P):

    def fun(T):
            
        return P - sol_press(x_, T)

    if tempsolver == 'brenth':

        return opt.brenth(fun, lo_temp, hi_temp)

    elif tempsolver == 'newton':

        return opt.newton(fun, hi_temp)
        
        
def vap_press(y_, T):

    def fun(x):
        
        return vap_comp_T(x, T) - y_

    if compsolver == 'brenth':

        x = opt.brenth(fun, 0, 1)

    elif compsolver == 'newton':

        x = opt.newton(fun, 1)
    
    return sol_press(x, T)
        

def vap_temp(y_, P):
    
    def fun(x):
        
        return vap_comp_P(x, P) - y_

    if compsolver == 'brenth':

        x = opt.brenth(fun, 0, 1)

    elif compsolver == 'newton':

        x = opt.newton(fun, 1)
    
    return sol_temp(x, P)

        
def vap_comp_T(x_, T):
        
    ac_ = ac(x_, T)
    
    return hp_vap_press(T)*x_*ac_[0] / sol_press(x_, T)
        
        
def vap_comp_P(x_, P):
        
    T = sol_temp(x_, P)

    return vap_comp_T(x_, T)


def cond_T(y_, T):
    def fun(x):

        return y_ - vap_comp_T(x, T)

    if compsolver == 'brenth':

        return opt.brenth(fun, 0, 1)

    elif compsolver == 'newton':

        return opt.newton(fun, 1)


def cond_P(y_, P):
    def fun(x):

        return y_ - vap_comp_P(x, P)

    if compsolver == 'brenth':

        return opt.brenth(fun, 0, 1)

    elif compsolver == 'newton':

        return opt.newton(fun, 1)


def sol_comp(P_, T_):

        def fun(x):

            return sol_press(x, T_) - P_

        if compsolver == 'brenth':

            return opt.brenth(fun, 0, 1)

        elif compsolver == 'newton':

            return opt.newton(fun, 1)


def vap_comp(P_, T_):

        x = sol_comp(P_, T_)

        return vap_comp_T(x, T_)


############## Energy functions ##############

def vap_heat_T(x, T):

    global hv

    w = m2w(x)

    return molar_mass(x) * hv(w, T)[0][0]


def vap_heat_P(x, P):
    
    T = sol_temp(x, P)

    return vap_heat_T(x, T)


def vap_heat(x, T):

    hpvh = 6.53092518e+04 -4.49353551e+01*T -2.49057846e-03*T**2
    wvh = 5.51878775e+04 -3.22161064e+01*T -1.78570870e-02*T**2

    return x*hpvh + (1-x)*wvh - mix_heat(x, T)


def heatcap(x, T):

    def shapefun(Z):

        A = 0.99991636
        B = -0.07919912
        C =  0.25431847
        D = -0.38459015
        E = 0.29386685
        F = -0.0845772

        return A + B*Z + C*Z**2 + D*Z**3 + E*Z**4 + F*Z**5

    def whc(T):

        Tf = 273.15
        Tb = 373.15

        Z = (T - Tf)/(Tb - Tf)

        return shapefun(Z) * 75.969255

    def hphc(T):

        Tf = 272.72
        Tb = 423.15

        Z = (T - Tf)/(Tb - Tf)
        Z0 = (311.9 - Tf)/(Tb - Tf)

        return shapefun(Z) * 89.305 / shapefun(Z0)

    def der_mix(T):

        return mix_heat(x, T)

    return x*hphc(T) + (1-x)*whc(T) + derivative(der_mix, T)


def eq_vap_heat(x, T):

    A = -3.72176514e+03
    B = -1.69363541e+03
    C = 1.80999213e+03
    D = -2.94864163e+04
    E = 2.49618569e+02
    F = -7.07502142e-01
    G = 6.75549199e-04
    H = 7.20630910e-08

    y = vap_comp_T(x, T)

    delta = A + 2*(B - A)*x + 3*(C-B)*x**2 - 4*C*x**3 + ((1-2*x)*np.log(x + H) + (x-x**2)/(x+H))*(D + E*T + F*T**2 + G*T**3)

    return vap_heat(x, T) - (x-y)*delta


def eq_cond_heat(y, T):

    A = -3.72176514e+03
    B = -1.69363541e+03
    C = 1.80999213e+03
    D = -2.94864163e+04
    E = 2.49618569e+02
    F = -7.07502142e-01
    G = 6.75549199e-04
    H = 7.20630910e-08

    x = cond_T(y, T)

    delta = A + 2*(B - A)*x + 3*(C-B)*x**2 - 4*C*x**3 + ((1-2*x)*np.log(x + H) + (x-x**2)/(x+H))*(D + E*T + F*T**2 + G*T**3)

    return vap_heat(x, T) - (x-y)*delta


############## Numerical functions ############## 

def quick_condensate(y0, P, d):

    def fun(y_, delta):

        return (y_ - cond_P(y_, P)) / (1 - delta)

    D = [0, d]

    return [odeint(fun, y0, D)[1][0], d]
