Bloc v 0.4

This library was created and developed for the purpose of establishing proof of concept to a newfound block method of finding fixed state in a fractionating system.
 

class Bit(size, x)
	
	An instance of this class serves as a container for two parameters.
	It represents a small bit of solution exchanged between trays during distillation.

	It is described by 2 parameters:

		Bit.size represents relative molar size of exchanged matter.
	
		Bit.x represents molar concentration of HP in bit.



class Boiler(x, P)

	An instance of this class serves as a source of vapour bits.
	It represents the boiling vessel in a distillation system
	
	It is described by 2 parameters:

		Boiler.x represents concentration of HP in liquid inside of the boiler.

		Boiler.P represents pressure in the system.

	Its interaction with the system is described by 2 variables:

		Boiler.outVapBit represents a bit of vapour emerging from the boiling vessel. It is always of size 1.

		Boiler.inLiqBit represents a bit of liquid returning to the boiling vessel.

	It has 2 methods:

		Boiler.sendVapBit():
		
			Sets Boiler.outVapBit variable to a Bit of the following parameters:
		
			Boiler.sendVapBit.size = 1
			Boiler.sendVapBit.x = vap_comp_P(Boiler.x, Boiler.P)

		Boiler.recLiqBit(_inLiqBit):

			Sets Boiler.inLiqBit to _inLiqBit.


	Instances of Boiler class are printable.



class Tray(x, P, size, model)


	An instance of this class models a single theoretical tray in fractionating column.

	It contains 4 parameters:

		Tray.x represents concentration of HP in liquid on the tray.

		Tray.P represents pressure in the system.

		Tray.size represents molar capacity of the tray.

		Tray.model contains evaporation heat model - 'm_cab_thi' or 'var_vap_heat'.

	Its interaction with the system is described by 4 parameters:

		Tray.outVapBit represents a vapour bit emerging from the tray.

		Tray.inVapBit represents a vapour bit received from the tray below.

		Tray.outLiqBit represents a liquid bit flowing downwards from the tray.

		Tray.inLiqBit represents a liquid bit flowing downwards onto the tray.

	It has 5 methods:

		Tray.recVapBit(_inVapBit):

			Sets Tray.inVapBit to _inVapBit.
		
		Tray.recLiqBit(_inLiqBit):

			Sets Tray.inLiqBit to _inLiqBit.	

		Tray.sendVapBit():

			Sets Tray.outVapBit to a Bit of the following parameters:

			Tray.outVapBit.x = vap_comp_P(Tray.x, Tray.P)
			Tray.outVapBit.size = S

			Where S depends on model.

			If Tray.model = 'm_cab_thi': S = Tray.inVapBit.size
			If Tray.model = 'var_vap_heat': S = Tray.inVapBit.size * k

			Where k is the ratio of condensation heat of incoming vapour to evaporation heat of tray liquid.

		Tray.sendLiqBit():

			Sets Tray.outLiqBit to a Bit of the following parameters:

			Tray.outLiqBit.x = Tray.x
			Tray.outLiqBit.size = Tray.inLiqBit.size + Tray.inVapBit.size - Tray.outVapBit.size

		Tray.process()

			Processes buffered bits to update composition of contained liquid.
	
	Instances of Tray class are printable.
	


class Reflux(R, T, P, type, ref):

	This class represents reflux component in the system.

	It contains 5 parameters:

		Reflux.R represents reflux coefficient.

		Reflux.T represents temperature to which vapour is cooled, if partial reflux is considered.

		Reflux.P represents pressure in the system.

		Reflux.type represents type of reflux: 'divided'/'partial'

		Reflux.ref represents reference parameter of the reflux: 'T' for temperature, 'R' for reflux coefficient.

	
	Its interaction with the system is described by 4 parameters:

		Tray.inVapBit represents input vapour bit.

		Tray.outVapBit represents output liquid bit.

	It has 2 methods:

		Tray.recVapBit(_inVapBit):

			Sets Tray.inVapBit to _inVapBit

		Tray.sendLiqBit():

			Sets Tray.outLiqBit to a Bit of parameters corresponding to Reflux.type and Reflux.ref variables.


