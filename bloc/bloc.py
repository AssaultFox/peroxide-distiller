from peroxylib import *
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d as i1d

class Bit:

    def __init__(self, _size=0.5, _x=0.5):
        self.size = _size
        self.x = _x


class Boiler:

    def __init__(self, _x, _P):
        self.x = _x
        self.P = _P
        self.outVapBit = Bit(0, 0)

    def __str__(self):
        X1 = self.x * 100
        Y1 = vap_comp_P(self.x, self.P) * 100
        T1 = sol_temp(self.x, self.P)
        P1 = self.P

        F2 = self.inLiqBit.size * 100
        X2 = self.inLiqBit.x * 100

        F3 = 100 - F2
        X3 = (100 * Y1 - F2 * X2) / F3

        return "###### BOILER ######\nX[%%]: %.2f\nY[%%]: %.2f\nT[K]: %.1f\nP[Pa]: %.0f\nRETURN STREAM:\nFlow[%%]: %.2f\nX[%%]: %.2f\nRESULTANT STREAM OUT:\nFlow[%%]: %.2f\nX[%%]: %.2f" % (
        X1, Y1, T1, P1, F2, X2, F3, X3)

    def sendVapBit(self):
        S = 1
        X = vap_comp_P(self.x, self.P)
        self.outVapBit = Bit(S, X)

    def recLiqBit(self, _inLiqBit):
        self.inLiqBit = _inLiqBit


class Tray:

    def __init__(self, _x, _P, _size, _model):

        self.x = _x
        self.P = _P
        self.size = _size
        self.model = _model
        self.outVapBit = Bit(0, 0)
        self.outLiqBit = Bit(0, 0)

    def __str__(self):

        return "###### TRAY ######\nX[%%]: %.2f\n" % (100 * self.x)

    def recVapBit(self, _inBit):

        self.inVapBit = _inBit

    def recLiqBit(self, _inBit):

        self.inLiqBit = _inBit

    def sendVapBit(self):

        if self.model == 'var_vap_heat':
            X = vap_comp_P(self.x, self.P)
            S = self.inVapBit.size * cond_heat_P(self.inVapBit.x, self.P) / vap_heat_P(self.x, self.P)
            self.outVapBit = Bit(S, X)

        if self.model == 'm_cab_thi':
            X = vap_comp_P(self.x, self.P)
            S = self.inVapBit.size
            self.outVapBit = Bit(S, X)

    def sendLiqBit(self):

        X = self.x
        S = self.inLiqBit.size + self.inVapBit.size - self.outVapBit.size
        self.outLiqBit = Bit(S, X)

    def process(self):

        self.x = (
                 self.x * self.size + self.inVapBit.size * self.inVapBit.x + self.inLiqBit.size * self.inLiqBit.x) / self.size
        self.x = (
                 self.x * self.size - self.outVapBit.size * self.outVapBit.x - self.outLiqBit.size * self.outLiqBit.x) / self.size


class Reflux:

    def __init__(self, _R, _P, _type):

        self.R = _R
        self.P = _P
        self.type = _type

    def __str__(self):

        if self.type == 'divided':
            R1 = self.R

            F2 = self.inVapBit.size * 100
            X2 = self.inVapBit.x * 100

            F3 = R * F2
            X3 = X2

            F4 = (1 - R) * F2
            X4 = (F2 * X2 - F3 * X3) / (F2 - F3)

            return "###### REFLUX (DIVIDED) ######\nR: %.2f\nVAP IN:\nFlow[%%]: %.2f\nX[%%]: %.2f\nLIQ OUT:\nFlow[%%]: %.2f\nX[%%]: %.2f\nVAP OUT:\nFlow[%%]: %.2f\nX[%%]: %.2f\n" % (
            R1, F2, X2, F3, X3, F4, X4)

        if self.type == 'partial':

            if self.ref == 'R':
                res = quick_condensate(self.inVapBit.x, self.P, self.R)

                R1 = res[1]

                F2 = self.inVapBit.size * 100
                X2 = self.inVapBit.x * 100

                F3 = R1 * F2
                X3 = (F2 * X2 - (1 - R1) * F2 * res[0] * 100) / F3

                F4 = (1 - R1) * F2
                X4 = res[0] * 100

                return "###### REFLUX (PARTIAL, R-REF) ######\nR: %.2f\nVAP IN:\nFlow[%%]: %.2f\nX[%%]: %.2f\nLIQ OUT:\nFlow[%%]: %.2f\nX[%%]: %.2f\nVAP OUT:\nFlow[%%]: %.2f\nX[%%]: %.2f\n" % (
                R1, F2, X2, F3, X3, F4, X4)

    def sendLiqBit(self):

        if self.type == 'divided':
            S = self.inVapBit.size * self.R
            X = self.inVapBit.x

        elif self.type == 'partial':
            res = quick_condensate(self.inVapBit.x, self.P, self.R)
            S = self.inVapBit.size * res[1]
            X = (self.inVapBit.x - (1 - res[1]) * res[0]) / res[1]

        self.outLiqBit = Bit(S, X)

    def recVapBit(self, _inVapBit):

        self.inVapBit = _inVapBit


class Column:

    def __init__(self, _n, traysize = 10, evaporation_model = "m_cab_thi", flood = 0, P = 10000, pressure_ratio = 1):

        self.n = _n
        self.trays = []

        for i in range(0, _n):

            self.trays.append(Tray(flood, P * (1. - (1. - pressure_ratio)*(i / (_n-1.))), traysize, evaporation_model))

    def vap_model(self, evaporation_model):

        for i in range(0, self.n):

            self.trays[i].model = evaporation_model

    def flood(self, _x):

            for i in range(0, self.n):
                self.trays[i].x = _x

    def __str__(self):

        stri = ""
        i = 1

        for tray in self.trays:

            stri = stri + "Tray " + str(i) + ": " + str(tray.x) + "\n"

            i = i + 1
        return stri


class Setup:

    def __init__(self, boiler = None, column = None, reflux = None):

        self.boiler = boiler
        self.column = column
        self.reflux = reflux
        self.steps = 0

    def resetSteps(self):

        self.steps = 0

    def step(self):

        print ("Taking step #", self.steps + 1)

        self.boiler.sendVapBit()

        self.column.trays[0].recVapBit(self.boiler.outVapBit)

        for i in range(0, self.column.n - 1):

            self.column.trays[i].sendVapBit()
            self.column.trays[i+1].recVapBit(self.column.trays[i].outVapBit)

        self.column.trays[self.column.n - 1].sendVapBit()
        self.reflux.recVapBit(self.column.trays[self.column.n - 1].outVapBit)
        self.reflux.sendLiqBit()
        self.column.trays[self.column.n - 1].recLiqBit(self.reflux.outLiqBit)

        for i in range(self.column.n - 1, 0, -1):

            self.column.trays[i].sendLiqBit()
            self.column.trays[i - 1].recLiqBit(self.column.trays[i].outLiqBit)

        self.column.trays[0].sendLiqBit()
        self.boiler.recLiqBit(self.column.trays[0].outLiqBit)

        for i in range(0, self.column.n):
            self.column.trays[i].process()

        self.steps = self.steps + 1

    def xCheck(self):

        delt = []
        avg = 0

        for i in range(0, self.column.n):

            tray = self.column.trays[i]
            delt.append(0)

            delt[i] = tray.inVapBit.x * tray.inVapBit.size + tray.inLiqBit.x * tray.inLiqBit.size
            delt[i] = delt[i] - (tray.outVapBit.x * tray.outVapBit.size + tray.outLiqBit.x * tray.outLiqBit.size)
            delt[i] = abs(delt[i])
            avg = avg + delt[i]

        d_max = max(delt)

        avg = avg / self.column.n

        print ("###### COLUMN X CHECK ######\nDelta Max: ", d_max, "\nDelta Mean: ", avg, "\n")

        return (avg, d_max)

    def snapColumn(self, filename = 'columnSnap', par = ".", nr = None, axis = (0, 1)):

        x = []
        y = []

        for i in range(0, self.column.n):
            x.append(i + 1)
            y.append(self.column.trays[i].x)

        X = np.array(x)
        Y = np.array(y)

        Xnew = np.linspace(X.min(), X.max(), 500)

        plt.figure(num=None, figsize=(10, 7), dpi=100, facecolor='w', edgecolor='k')
        plt.axis([1, self.column.n, axis[0], axis[1]])

        Ynew = i1d(X, Y)(Xnew)

        plt.plot(Xnew, Ynew, 'k')

        if "." in par:

            plt.plot(x, y, 'k.')

        if "v" in par or "V" in par:

            vaps = []

            for i in range(0, self.column.n):

                vaps.append(vap_comp_P(y[i], self.column.trays[i].P))

            VAPS = np.array(vaps)

            if "V" in par:

                plt.plot(x, VAPS, 'b.')

            VAPSnew = i1d(X, VAPS)(Xnew)

            plt.plot(Xnew, VAPSnew, "b")

        if "s" in par or "S" in par:

            streams = []

            for i in range(0, self.column.n):
                tray = self.column.trays[i]
                stream_ = tray.outVapBit.x * tray.outVapBit.size
                stream_ = stream_ - tray.inLiqBit.x * tray.inLiqBit.size
                stream_ = stream_ / (tray.outVapBit.size - tray.inLiqBit.size)
                streams.append(stream_)

            STREAMS = np.array(streams)

            if "S" in par:
                plt.plot(x, STREAMS, 'r.')

            STREAMSnew = i1d(X, STREAMS)(Xnew)

            plt.plot(Xnew, STREAMSnew, 'r')


        name = filename

        if nr != None:

            name = name + str(nr)

        name = name + ".png"

        plt.savefig("img/" + name)
        plt.show()

    def saveColumn(self, filename):

        a = [self.column]
        ats = np.array(a)

        np.save("sim_data/columns/" + filename, ats)

    def loadColumn(self, filename):

        self.column = np.load("sim_data/columns/" + filename + ".npy")[0]

    def saveAll(self, filename):

        a = [self.boiler, self.column, self.reflux, self.steps]
        ats = np.array(a)

        np.save("sim_data/systems/" + filename, ats)

    def loadAll(self, filename):

        loaded = np.load("sim_data/systems/" + filename + ".npy")

        self.boiler = loaded[0]
        self.column = loaded[1]
        self.reflux = loaded[2]
        self.steps = loaded[3]


