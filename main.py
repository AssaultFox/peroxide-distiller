from peroxylib import *
from peroxylib import method as meth
from peroxyunits import *
from bloc import *

#   SYSTEM  #
P = 10 * mbar()             # Pressure in the boiler
Boil_X = 0.95               # Composition of boiler liquid
N = 15                      # Number of theoretical trays
model = 'm_cab_thi'         # 'var_vap_heat'/'m_cab_thi' - variable evaporation heat/Mc-T method

#   REFLUX  #
R = 0.8825

#   CONTROL #
i_max = 5000

raport = True
i_raport = 1000

switch_model = True
i_model = 22500

switch_type = False
i_partial = 24000

snapshot = False
i_snapshot = 1000

#   NUMERICS  #
meth(compsolv = 'newton', tempsolv = 'newton', presssolv= 'newton')
flood = Boil_X/2            # Exit point for computation of tray compositions
tray_size = 15              # Tray capacity in boiler output bits

#   CONTROL FUNCTIONS   #
i_cf1 = [i_raport, 'every', raport]
i_cf2 = [i_partial, 'once', switch_type]
i_cf3 = [i_model, 'once', switch_model]
i_cf4 = [i_snapshot, 'every', snapshot]

def cf1(iter, S):

    if i_cf1[2] & (iter % i_cf1[0] == 0):

        S.xCheck()

        if i_cf1[1] == 'once':

            i_cf1[2] = False

def cf2(iter, S):

     if i_cf2[2] & (iter % i_cf2[0] == 0):

         print ("###### SWITCHING REFLUX TYPE TO PARTIAL ######")

         S.reflux.type = 'partial'

         if i_cf2[1] == 'once':

            i_cf2[2] = False

def cf3(iter, S):

    if i_cf3[2] & (iter % i_cf3[0] == 0):

        print ("###### SWITCHING HEAT EXCHANGE MODEL TO VAR_VAP_HEAT ######")

        S.column.vap_model("var_vap_heat")

        if i_cf3[1] == 'once':

            i_cf3[2] = False

def cf4(iter, S):

    if i_cf4[2] & (iter % i_cf4[0] == 0):

        S.snapColumn("snapshots/column", par = ".SV")

        if i_cf4[1] == 'once':

            i_cf4[2] = False

#   LOOP  #

def initialize():

    global B
    global C
    global R
    global S

    B = Boiler(Boil_X, P)
    C = Column(N, traysize=tray_size, evaporation_model=model, P=P, pressure_ratio=1)
    Rf = Reflux(R, P, 'divided')
    S = Setup(boiler = B, column = C, reflux = Rf)
def loop():

    global S

    for i in range(1, i_max+1):

        S.step()
        cf1(i, S)
        cf2(i, S)
        cf3(i, S)
        cf4(i, S)

initialize()
loop()

S.snapColumn("Final", par = '.SV')
S.saveAll('result')